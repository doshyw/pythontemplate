clean:
	rm -rf tests/__pycache__/
	rm -rf src/*.egg-info/
	rm -rf build/
	rm -rf .mypy_cache/
	rm -f requirements.in
	rm -f dpcheck.html

clean-tox: clean
	rm -rf .tox/

clean-all: clean-tox
	rm -rf dist

format:
	tox -q -e format

lint:
	tox -q -e "pylama, bandit, dpcheck"

test:
	tox -q -e "reqs, pytest"

pre-build:
	tox -q -e "format, pylama, bandit, dpcheck, pytest"

helper-build:
	tox -q -e "reqs, format, pylama, bandit, dpcheck, pytest"
	tox -q -e build

helper-build-unsafe:
	tox -q -e reqs
	tox -q -e build

build: helper-build clean

build-unsafe: helper-build-unsafe clean
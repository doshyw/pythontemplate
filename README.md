# Introduction
A template package setup to be built using the build Python library in 3.10.

# Requirements
The use of this template requires the following to be installed and available:
- **Python 3.10**: (download from [latest](https://www.python.org/downloads/release/python-31010/)).
- **Pip**: (run `python -m ensurepip --upgrade`).
- **Tox**: (install via `pip install tox`).
- **Make**:
  - *Windows*: Install using [Chocolatey](https://chocolatey.org/install) via `choco install make` or install [Windows Subsystem for Linux](https://learn.microsoft.com/en-us/windows/wsl/install).
  - *Linux or MacOS*: Comes pre-installed.

# Developing with this Package
This package should not be built or deployed, it is only intended to serve as a base template for building other packages from. For packages that build on this template, note the following:

## Running Tests

The following command will run the configured formatters, linters, and unit tests.
```
make test
```
Currently, the following tools are in use:
- [Black](https://github.com/psf/black) (a PEP 8 compliant opinionated formatter).
- [docformatter](https://github.com/PyCQA/docformatter) (a PEP 257 docstring formatter)
- [Pylama](https://github.com/klen/pylama), which wraps the following:
  - [eradicate](https://github.com/wemake-services/eradicate) (a Python utility to flag commented-out code),
  - [isort](https://github.com/PyCQA/isort) (a Python utility to sort imports),
  - [Mypy](https://github.com/python/mypy) (a Python static type checker),
  - [pycodestyle](https://github.com/PyCQA/pycodestyle) (a PEP 8 style guide checker),
  - [pydocstyle](https://github.com/PyCQA/pydocstyle) (a PEP 257 docstring style checker),
  - [PyFlakes](https://github.com/PyCQA/pyflakes) (a Python error checker),
  - [Pylint](https://github.com/PyCQA/pylint) (a Python static code analyser),
  - [Radon](https://github.com/rubik/radon) (a Python metrics generator - may need to be replaced with [Xenon](https://github.com/rubik/xenon) for blocking purposes),
  - [Vulture](https://github.com/jendrikseipp/vulture) (a Python utility to flag unreferenced code).
- [Bandit](https://github.com/PyCQA/bandit) (a Python utility for identifying common security issues).
- [OWASP Dependency Check](https://github.com/jeremylong/DependencyCheck) (an open-source utility for Software Composition Analysis)

## Building

The following command will build the wheel for the package and place it in the `dist/` folder:

```
make build
```

This will, by default, run all of the tests that `make test` runs. This can be skipped (not recommended) by running the following command:

```
make build-unsafe
```